#!/bin/bash
#set -x

function inizio() {
	dataLog=`date +%Y%m%d_%H%M%S`	
	SCRIPT_OUT=$DIR_LOG/$LOG\_$dataLog.out
	NOME_SCRIPT=$0
	dataSt=`date +%d/%m/%Y\ -\ %X`
	echo "***************************************************************" >>$SCRIPT_OUT
	echo "$NOME_SCRIPT inizio $dataSt" >>$SCRIPT_OUT
}

function fine() {
	dataSt=`date +%d/%m/%Y\ -\ %X`
	echo "$0 fine $dataSt" >>$SCRIPT_OUT
	echo "***************************************************************" >>$SCRIPT_OUT
	#echo "Return Code  $rc"
	#echo "Return Code  $rc" >>$SCRIPT_OUT
	exit $rc
}

function avvioProcesso {

	echo "REPORT_FILE" $SCRIPT_OUT
	echo "Avvio job Estrazione_JSONL_Quote_attive"

	$KITCHEN -file=$DIR_TRASFORMAZIONI/$TRASFORMAZIONE -level=$LOGLEVEL >>$SCRIPT_OUT;echo "Fine"

	rc=$?
	return $rc
}

function controlloFile() {
	file_errori=$(cat $SCRIPT_OUT | grep "ERROR" | wc -l)
	if [ $file_errori != "0" ]; then
		echo "ERRORE DURANTE L'ESECUZIONE DEL JOB Estrazione_JSONL_Quote_attive"
		rc=1
		return $rc
	else
		echo "L'ESECUZIONE DEL JOB Estrazione_JSONL_Quote_attive HA AVUTO SUCCESSO"
		rc=0
		return $rc
	fi
}

#definizioni
DIR_TRASFORMAZIONI=/opt/dvsu/x/bin
LOGLEVEL=Basic

#variabili della trasformazione
TRASFORMAZIONE=impianto/Estrazione_JSONL_Quote_attive.kjb
FILE_LOCK=$DIR_LOG/$TRASFORMAZIONE.filelock
LOG=Impianto-Estrazione_JSONL_Quote_attive

#scrivo nel log l'avvio del processo
inizio

#avvio il processo
avvioProcesso $*
rc=$?

if [ $rc -ne 0 ]
then
	echo "\n Errore nel processo rc=$rc" >>$SCRIPT_OUT
fi

controlloFile

#fine processo
fine

exit $rc
